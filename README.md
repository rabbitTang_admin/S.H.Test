## 多语言开发文档支持

| 语言           | 链接                                                         | 版本 |
| -------------- | ------------------------------------------------------------ | ---- |
| 英语\|English  | [English VERSION](https://gitee.com/rabbitTang_admin/S.H.Test/blob/master/README_ENG.md) | v1.0 |
| 中文\|Chinese  | [中文 VERSION](https://gitee.com/rabbitTang_admin/S.H.Test/blob/master/README.md) | v1.0 |
| 日语\|Japanese | [日语 VERSION](https://gitee.com/rabbitTang_admin/S.H.Test/blob/master/README_JPN.md) | v1.0 |

## 关于开源

本项目已与``2020年11月11日``开源，采用**[GPL-2.0](https://gitee.com/rabbitTang_admin/S.H.Test/blob/master/LICENSE)**许可证，各取所需。

## 关于跨平台

~~本项目已通过`Java`和`C++`混合编程实现跨平台，理论支持Mac、Linux、Windows等平台~~


暂时放弃跨平台（很多API需要重写才能实现功能，日后有空再维护）

## S.H.HOME

基于企想Bizideal智能家居安装与维护平台Android端的控制终端设备软件。

## 用户文档

编写中

## 赞赏

| 微信                                                         | 支付宝                                                       |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![微信](https://yp.nyanon.online/index.php?user/publicLink&fid=2de0cWutSBNLwAHf2wKj9jTu1p7CdgvG7frqYDYx5hxy0-olktzX-pqFUu_ERrtigTfFQjOA_v5-DVtk-XrE9aiIZfLKXOIVys5HDdhstpaNSyzMXOPdMC4fJ9QMaESHcnjusI71vhlwO-Zcn2a4&file_name=/wechatpay.jpg) | ![支付宝](https://yp.nyanon.online/index.php?user/publicLink&fid=fbc5CTYExmGz2jwK5RCh_7AH_VBreib4I37_naefmWF-PijPGScnC933sBCbb8IG8t7hjKBiFyj6Ej74mk6dyu5ZrrzqRzyOXmmQKEzesejamTV9YZm62xS9TstSW0t2iPwyQGPJeJMXgGXA&file_name=/alipay.jpg) |





