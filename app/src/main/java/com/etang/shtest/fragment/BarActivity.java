package com.etang.shtest.fragment;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bizideal.smarthome.socket.Utils.IOcwBIZ;
import com.bizideal.smarthome.socket.Utils.OfflineVoiceUtils;
import com.bizideal.smarthome.socket.Utils.SpUtils;
import com.etang.shtest.R;
import com.etang.shtest.dialogs.DialogDebugs;
import com.etang.shtest.dialogs.DialogMessage;
import com.etang.shtest.tools.AppConfig;
import com.etang.shtest.tools.DiyToast;
import com.etang.shtest.tools.MyViewPager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;

/**
 * 滑动界面FragmentActivity
 */
public class BarActivity extends FragmentActivity {

    //自定义的viewpager
    MyViewPager pager;
    //自定义的pagerchacker
    PagerChecker pagerChecker;
    //继承Fragment的List，用于存放Fragment碎片
    List<Fragment> list_fragment = new ArrayList<>();
    //RadioButton单选按钮-----设备、控制、智能（暂时没用）、我的
    private RadioButton ra_base, ra_control, ra_value, ra_me;
    //语音按钮
    private ImageView iv_voise;
    //界面title标题
    private TextView tv_bar_title;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar);
        //绑定控件
        initView();
        //初始化web相关的服务
        AppConfig.web_init(BarActivity.this);
        //设置默认title标题
        tv_bar_title.setText(getString(R.string.string_base));
//        list_fragment.add(new ValueFragment());
        //向List中添加Fragment碎片
        list_fragment.add(new BaseFragment());//设备
        list_fragment.add(new ControlFragment());//控制
        list_fragment.add(new MeFragment());//我的
        //绑定自定义ViewPager
        pager = (MyViewPager) findViewById(R.id.pager);
        pagerChecker = new PagerChecker(getSupportFragmentManager());
        //设置Adapter
        pager.setAdapter(pagerChecker);
        pager.setOffscreenPageLimit(list_fragment.size());
        //设置pager切换事件监听
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.e("TAG", "onPageScrolled: " + position);
                switch (position) {
                    //页面0
                    case 0:
                        ra_base.setChecked(true);
                        tv_bar_title.setText(getString(R.string.string_base));
                        break;
                    //页面1
                    case 1:
                        ra_control.setChecked(true);
                        tv_bar_title.setText(getString(R.string.string_control));
                        break;
                    //页面2
                    case 2:
                        ra_me.setChecked(true);
                        tv_bar_title.setText(getString(R.string.string_my));
                        break;
                }
            }

            @Override
            public void onPageSelected(int position) {
                Log.e("TAG", "onPageSelected: " + position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.e("TAG", "onPageScrollStateChanged: " + state);
            }
        });
        //设备单选
        ra_base.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(0);
            }
        });
        //控制单选
        ra_control.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(1);
            }
        });
        //我的单选
        ra_me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(2);
            }
        });
        //语音识别
        iv_voise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //使用try防止报错
                try {
                    //设置语音识别APPID
                    OfflineVoiceUtils.APPID = "5fac85a6";
                    //设置Context
                    OfflineVoiceUtils.getInstance(BarActivity.this).setContext(BarActivity.this);
                    //创建语音识别的dialog
                    OfflineVoiceUtils.getInstance(BarActivity.this).build();
                    //创建接收IOcwBIZ返回值的方法
                    OfflineVoiceUtils.getInstance(BarActivity.this).startListening(new IOcwBIZ() {
                        @Override
                        public void onResult(String s) {
                            //弹出框提示
                            DialogMessage.showdialog(BarActivity.this, "语音输入结果：" + s);
                        }
                    });
                } catch (Exception e) {
                    //弹出错误提示
                    DialogDebugs.showdialog(BarActivity.this, "语音初始化错误！", e.toString());
                }
            }
        });
        //检查设置语言
        setLanguage();
    }

    /**
     * 绑定控件
     */
    private void initView() {
        iv_voise = (ImageView) findViewById(R.id.iv_voice_start);
        ra_base = (RadioButton) findViewById(R.id.ra_base);
        ra_control = (RadioButton) findViewById(R.id.ra_control);
        ra_me = (RadioButton) findViewById(R.id.ra_my);
        ra_value = (RadioButton) findViewById(R.id.ra_smart);
        tv_bar_title = (TextView) findViewById(R.id.tv_bar_title);
    }

    /**
     * PagerChecker用来装载Fragment碎片
     */
    public class PagerChecker extends FragmentPagerAdapter {

        public PagerChecker(@NonNull FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return list_fragment.get(position);
        }

        @Override
        public int getCount() {
            return list_fragment.size();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void setLanguage() {
        //读取SharedPreferences数据，默认选中第一项
        int language = Integer.valueOf(SpUtils.getValue(getApplicationContext(), "language", null));
        //根据读取到的数据，进行设置
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        switch (language) {
            case 0:
                //自动获取
                configuration.setLocale(Locale.getDefault());
                break;
            case 1:
                //中文
                configuration.setLocale(Locale.CHINESE);
                break;
            case 2:
                //英文
                configuration.setLocale(Locale.ENGLISH);
                break;
            case 3:
                //日文
                configuration.setLocale(Locale.JAPANESE);
                break;
            default:
                break;
        }
        resources.updateConfiguration(configuration, displayMetrics);
    }

}