package com.etang.shtest.fragment;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.bizideal.smarthome.socket.ConstantUtil;
import com.bizideal.smarthome.socket.ControlUtils;
import com.bizideal.smarthome.socket.Utils.SpUtils;
import com.etang.shtest.R;
import com.etang.shtest.dialogs.DialogDebugs;
import com.etang.shtest.tools.AppConfig;

import java.util.Locale;

/**
 * 设备控制界面
 */
public class ControlFragment extends Fragment {
    //ToggButton按钮：射灯1、射灯2、风扇、报警灯、电视、DVD、空调、门禁
    private ToggleButton tg_lamp_1, tg_lamp_2, tg_fan, tg_warm, tg_tv, tg_dvd, tg_kt, tg_door;
    //ImageView图片：窗帘开、窗帘关、窗帘停止、射灯1、射灯2、报警灯、风扇、电视、DVD、门禁、空调
    private ImageView iv_cur_open, iv_cur_cls, iv_cur_stop, iv_lamp_1, iv_lamp_2, iv_warm, iv_fan, iv_tv, iv_dvd, iv_door, iv_kongtiao;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //设置View
        View view = inflater.inflate(R.layout.fragment_control, null, false);
        //绑定控件
        initView(view);
        //窗帘停止
        iv_cur_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ControlUtils.control(ConstantUtil.Relay, AppConfig.BORDID_CUR, ConstantUtil.CmdCode_3, ConstantUtil.Channel_3, ConstantUtil.Channel_3);
            }
        });
        //窗帘打开
        iv_cur_open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ControlUtils.control(ConstantUtil.Relay, AppConfig.BORDID_CUR, ConstantUtil.CmdCode_3, ConstantUtil.Channel_2, ConstantUtil.Channel_2);
            }
        });
        //窗帘关闭
        iv_cur_cls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ControlUtils.control(ConstantUtil.Relay, AppConfig.BORDID_CUR, ConstantUtil.CmdCode_3, ConstantUtil.Channel_3, ConstantUtil.Channel_3);
            }
        });
        //报警灯
        tg_warm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    iv_warm.setImageResource(R.drawable.ic_warm_on);
                    ControlUtils.control(ConstantUtil.Relay, AppConfig.BORDID_WARM, ConstantUtil.CmdCode_1, ConstantUtil.Channel_ALL, ConstantUtil.Open);
                } else {
                    iv_warm.setImageResource(R.drawable.ic_warm_off);
                    ControlUtils.control(ConstantUtil.Relay, AppConfig.BORDID_WARM, ConstantUtil.CmdCode_1, ConstantUtil.Channel_ALL, ConstantUtil.Close);
                }
            }
        });
        //电视
        tg_tv.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    iv_tv.setImageResource(R.drawable.ic_tv_on);
                    ControlUtils.control(ConstantUtil.Infrared, AppConfig.BORDID_INF, "1", ConstantUtil.Channel_1, ConstantUtil.Open);
                } else {
                    iv_tv.setImageResource(R.drawable.ic_tv_off);
                    ControlUtils.control(ConstantUtil.Infrared, AppConfig.BORDID_INF, "1", ConstantUtil.Channel_1, ConstantUtil.Open);
                }
            }
        });
        //射灯2
        tg_lamp_2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    iv_lamp_2.setImageResource(R.drawable.ic_lamp_on);
                    ControlUtils.control(ConstantUtil.Relay, AppConfig.BORDID_LAMP, ConstantUtil.CmdCode_1, ConstantUtil.Channel_2, ConstantUtil.Open);
                } else {
                    iv_lamp_2.setImageResource(R.drawable.ic_lamp_off);
                    ControlUtils.control(ConstantUtil.Relay, AppConfig.BORDID_LAMP, ConstantUtil.CmdCode_1, ConstantUtil.Channel_2, ConstantUtil.Close);
                }
            }
        });
        //射灯1
        tg_lamp_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    iv_lamp_1.setImageResource(R.drawable.ic_lamp_on);
                    ControlUtils.control(ConstantUtil.Relay, AppConfig.BORDID_LAMP, ConstantUtil.CmdCode_1, ConstantUtil.Channel_1, ConstantUtil.Open);
                } else {
                    iv_lamp_1.setImageResource(R.drawable.ic_lamp_off);
                    ControlUtils.control(ConstantUtil.Relay, AppConfig.BORDID_LAMP, ConstantUtil.CmdCode_1, ConstantUtil.Channel_1, ConstantUtil.Close);
                }
            }
        });
        //空调
        tg_kt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    iv_kongtiao.setImageResource(R.drawable.ic_kt_on);
                    ControlUtils.control(ConstantUtil.Infrared, AppConfig.BORDID_INF, "2", ConstantUtil.Channel_1, ConstantUtil.Open);
                } else {
                    iv_kongtiao.setImageResource(R.drawable.ic_kt_off);
                    ControlUtils.control(ConstantUtil.Infrared, AppConfig.BORDID_INF, "2", ConstantUtil.Channel_1, ConstantUtil.Open);
                }
            }
        });
        //风扇
        tg_fan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    iv_fan.setImageResource(R.drawable.ic_fan_on);
                    ControlUtils.control(ConstantUtil.Relay, AppConfig.BORDID_FAN, ConstantUtil.CmdCode_1, ConstantUtil.Channel_ALL, ConstantUtil.Open);
                } else {
                    iv_fan.setImageResource(R.drawable.ic_fan_off);
                    ControlUtils.control(ConstantUtil.Relay, AppConfig.BORDID_FAN, ConstantUtil.CmdCode_1, ConstantUtil.Channel_ALL, ConstantUtil.Close);
                }
            }
        });
        //DVD
        tg_dvd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    iv_dvd.setImageResource(R.drawable.ic_dvd_on);
                    ControlUtils.control(ConstantUtil.Infrared, AppConfig.BORDID_INF, "3", ConstantUtil.Channel_1, ConstantUtil.Open);
                } else {
                    iv_dvd.setImageResource(R.drawable.ic_dvd_off);
                    ControlUtils.control(ConstantUtil.Infrared, AppConfig.BORDID_INF, "3", ConstantUtil.Channel_1, ConstantUtil.Open);
                }
            }
        });
        //门禁
        tg_door.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    iv_door.setImageResource(R.drawable.ic_door_on);
                    ControlUtils.control(ConstantUtil.RFID_Door, AppConfig.BORDID_DOOR, ConstantUtil.CmdCode_2, ConstantUtil.Channel_1, ConstantUtil.Open);
                } else {
                    iv_door.setImageResource(R.drawable.ic_door_off);
                    ControlUtils.control(ConstantUtil.RFID_Door, AppConfig.BORDID_DOOR, ConstantUtil.CmdCode_2, ConstantUtil.Channel_1, ConstantUtil.Close);
                }
            }
        });
        //设置语言
        setLanguage();
        return view;
    }

    /**
     * 读取语言设置
     */
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void setLanguage() {
        //读取SharedPreferences数据，默认选中第一项
        int language = Integer.valueOf(SpUtils.getValue(getActivity(), "language", null));
        //根据读取到的数据，进行设置
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        switch (language) {
            case 0:
                //自动获取
                configuration.setLocale(Locale.getDefault());
                break;
            case 1:
                //中文
                configuration.setLocale(Locale.CHINESE);
                break;
            case 2:
                //英文
                configuration.setLocale(Locale.ENGLISH);
                break;
            case 3:
                //日文
                configuration.setLocale(Locale.JAPANESE);
                break;
            default:
                break;
        }
        resources.updateConfiguration(configuration, displayMetrics);
    }

    /**
     * 绑定控件
     *
     * @param view 继承View
     */
    private void initView(View view) {
        tg_door = (ToggleButton) view.findViewById(R.id.tg_door);
        tg_dvd = (ToggleButton) view.findViewById(R.id.tg_dvd);
        tg_fan = (ToggleButton) view.findViewById(R.id.tg_fan);
        tg_kt = (ToggleButton) view.findViewById(R.id.tg_kongtiao);
        tg_lamp_1 = (ToggleButton) view.findViewById(R.id.tg_lamp_1);
        tg_lamp_2 = (ToggleButton) view.findViewById(R.id.tg_lamp_2);
        tg_tv = (ToggleButton) view.findViewById(R.id.tg_tv);
        tg_warm = (ToggleButton) view.findViewById(R.id.tg_warm);
        iv_cur_cls = (ImageView) view.findViewById(R.id.iv_cur_cls);
        iv_cur_open = (ImageView) view.findViewById(R.id.iv_cur_open);
        iv_cur_stop = (ImageView) view.findViewById(R.id.iv_cur_stop);
        iv_door = (ImageView) view.findViewById(R.id.iv_door);
        iv_dvd = (ImageView) view.findViewById(R.id.iv_dvd);
        iv_fan = (ImageView) view.findViewById(R.id.iv_fan);
        iv_kongtiao = (ImageView) view.findViewById(R.id.iv_kongtiao);
        iv_lamp_1 = (ImageView) view.findViewById(R.id.iv_lamp_1);
        iv_lamp_2 = (ImageView) view.findViewById(R.id.iv_lamp_2);
        iv_tv = (ImageView) view.findViewById(R.id.iv_tv);
        iv_warm = (ImageView) view.findViewById(R.id.iv_warm);
    }
}
