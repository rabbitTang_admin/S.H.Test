package com.etang.shtest;

import android.Manifest;
import android.app.Activity;
import android.app.AppComponentFactory;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.etang.shtest.tools.DiyToast;
import com.etang.shtest.tools.SavePermission;

public class WeleComeActivity extends AppCompatActivity {
    private Button btn_welecome_quanxian;
    private static final int BAIDU_READ_PHONE_STATE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welecome);
        btn_welecome_quanxian = (Button) findViewById(R.id.btn_welecome_quanxian);
        btn_welecome_quanxian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SavePermission.check_save_permission(WeleComeActivity.this)) {
                    startActivity(new Intent(WeleComeActivity.this, MainActivity.class));
                    overridePendingTransition(0, 0);//关闭跳转动画
                    finish();
                } else {
                    DiyToast.showToast(getApplicationContext(), "请授予权限");
                }
            }
        });
    }
}
