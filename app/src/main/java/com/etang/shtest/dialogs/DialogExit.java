package com.etang.shtest.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.etang.shtest.R;

/**
 * 显示退出信息的dialog
 */
public class DialogExit {
    /**
     * 显示dialog的方法
     *
     * @param activity 继承activity
     * @param message  要退出时显示的信息
     */
    public static void showdialog(Activity activity, String message) {
        //新建view
        View view = LayoutInflater.from(activity).inflate(R.layout.dialog_nomo, null, false);
        //新建alerdialog，继承自activity
        AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
        //设置view
        alertDialog.setView(view);
        //设置关闭点击弹出框外部关闭弹出框功能
        alertDialog.setCanceledOnTouchOutside(false);
        //按钮1
        final Button btn_dialog_nomo_1 = (Button) view.findViewById(R.id.btn_dialog_nomo_1);
        //标题
        final TextView tv_dialog_nomo_title = (TextView) view.findViewById(R.id.tv_dialog_nomo_title);
        //信息
        final TextView tv_dialog_nomo_message = (TextView) view.findViewById(R.id.tv_dialog_nomo_message);
        //设置alerdialog标题
        tv_dialog_nomo_title.setText(activity.getString(R.string.dialog_info_title_nomo));
        //设置alerdialog信息
        tv_dialog_nomo_message.setText(message);
        //设置按钮1文本
        btn_dialog_nomo_1.setText(activity.getString(R.string.exit_system));
        //设置按钮1点击事件
        btn_dialog_nomo_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //关闭alerdialog
                alertDialog.dismiss();
                //关闭软件
                System.exit(0);
            }
        });
        //显示alerdialog
        alertDialog.show();
    }
}
