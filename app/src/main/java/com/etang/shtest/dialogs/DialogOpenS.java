package com.etang.shtest.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.etang.shtest.R;
import com.etang.shtest.tools.DiyToast;

/**
 * 显示开源协议的dialog
 */
public class DialogOpenS {
    /**
     * 显示dialog的方法
     *
     * @param activity 继承Activity
     * @param context  继承Context
     */
    public static void showToast(Activity activity, Context context) {
        //Toast提示稍等
        DiyToast.showToast(context, context.getString(R.string.string_web_plzstandby));
        //创建一个AlertDialog对象
        AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
        //设置Dialog的title标题
        alertDialog.setTitle(context.getString(R.string.string_web_kaiyuanxieyi));
        //创建一个view
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_opens, null, false);
        //给Dialog设置自定义View
        alertDialog.setView(view);
        //设置关闭点击弹出框外部关闭弹出框功能
        alertDialog.setCanceledOnTouchOutside(false);
        //绑定关闭按钮
        Button btn_open_cls = (Button) view.findViewById(R.id.btn_open_cls);
        //设置按钮文本
        btn_open_cls.setText(context.getString(R.string.button_cls));
        //绑定webview
        WebView webView = (WebView) view.findViewById(R.id.open_webview);
        //新建一个websetting对象
        WebSettings webSettings = webView.getSettings();
        //加载url网页
        webView.loadUrl("https://rabbittang_admin.gitee.io/s.h.test/LICENSE");
        //设置自适应 1
        webSettings.setUseWideViewPort(true);
        //设置自适应 2
        webSettings.setLoadWithOverviewMode(true);
        //设置连接监听
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        //关闭按钮
        btn_open_cls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //关闭dialog
                alertDialog.dismiss();
            }
        });
        //显示dialog
        alertDialog.show();
    }
}
