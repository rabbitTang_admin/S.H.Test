package com.etang.shtest;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import com.etang.shtest.dialogs.DialogMessage;
import com.etang.shtest.tools.AppConfig;
import com.etang.shtest.tools.DiyToast;
import com.etang.shtest.tools.MySQLiteHelper;

public class RegActivity extends AppCompatActivity {
    private EditText et_user, et_pass;
    private Button btn_con;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg);
        initView();
        btn_con.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_user.getText().toString().isEmpty() || et_pass.getText().toString().isEmpty()) {
                    DiyToast.showToast(getApplicationContext(), "不能有空白项");
                } else {
                    MySQLiteHelper.insert(getApplicationContext(), et_user.getText().toString(), et_pass.getText().toString());
                    overridePendingTransition(0, 0);//关闭跳转动画
                    finish();
                }
            }
        });
    }

    private void initView() {
        btn_con = (Button) findViewById(R.id.btn_reg_con);
        et_pass = (EditText) findViewById(R.id.et_reg_passward);
        et_user = (EditText) findViewById(R.id.et_reg_username);
    }

    public void reg_exit(View view) {
        finish();
    }
}