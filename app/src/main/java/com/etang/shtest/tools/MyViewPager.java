package com.etang.shtest.tools;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.viewpager.widget.ViewPager;

/**
 * 自定义的ViewPager
 */
public class MyViewPager extends ViewPager {
    /**
     * 构造方法
     *
     * @param context 继承Context
     */
    public MyViewPager(Context context) {
        super(context);
    }

    /**
     * 构造方法
     *
     * @param context 继承Context
     * @param attrs   继承AttributeSet
     */
    public MyViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    public void setCurrentItem(int item) {
        super.setCurrentItem(item, false);
    }
}
