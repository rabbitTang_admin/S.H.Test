package com.etang.shtest.tools;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

/**
 * 创建数据库
 */
public class MyDataBaseHelper extends SQLiteOpenHelper {
    /**
     * 构造方法
     *
     * @param context 继承context
     * @param name    数据库本地名
     * @param factory 数据库游标
     * @param version 数据库版本
     */
    public MyDataBaseHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //创建用于存放用户昵称、用户密码、用户手机号的表
        db.execSQL("create table user (_id integer primary key autoincrement,username text,passward text)");
        //初始化各种阈值（半成品）
        db.execSQL("create table smo_value (_id integer primary key autoincrement,value text)");//烟雾
        db.execSQL("create table temp_value_max (_id integer primary key autoincrement,value text)");//温度
        db.execSQL("create table temp_value_low (_id integer primary key autoincrement,value text)");//温度
        db.execSQL("create table ill_value_max (_id integer primary key autoincrement,value text)");//光照
        db.execSQL("create table ill_value_low (_id integer primary key autoincrement,value text)");//光照
        db.execSQL("create table co_value_max (_id integer primary key autoincrement,value text)");//co2
        db.execSQL("create table co_value_low (_id integer primary key autoincrement,value text)");//co2
        db.execSQL("create table pm_value_max (_id integer primary key autoincrement,value text)");//pm
        db.execSQL("create table pm_value_low (_id integer primary key autoincrement,value text)");//pm
        db.execSQL("create table hum_value_max (_id integer primary key autoincrement,value text)");//湿度
        db.execSQL("create table hum_value_low (_id integer primary key autoincrement,value text)");//湿度
        db.execSQL("create table green_mode (_id integer primary key autoincrement,value text)");//绿色模式
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}