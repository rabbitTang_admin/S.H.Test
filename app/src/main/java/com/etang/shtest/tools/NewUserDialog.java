package com.etang.shtest.tools;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;


import com.etang.shtest.R;
import com.etang.shtest.dialogs.DialogDebugs;

import java.lang.reflect.Field;
import java.security.MessageDigest;

/**
 * 收集并发送设备信息
 */
public class NewUserDialog {
    //方糖Server酱API
    private static String SKEY = "SCU66788Tac2bf7385575174e067c917d471e25365dd3983cec5ee";
    //推送消息到微信的Url
    private static String web_index = "sc.ftqq.com";

    /**
     * 对外调用方法（名字不重要）
     *
     * @param context 继承Context
     * @param info    继承一个String Info
     */
    public static void dialog_show(Context context, String info) {
        test(context, info);
    }

    /**
     * 对内调用方法
     *
     * @param context 继承Context
     * @param info    继承一个String Info
     */
    private static void test(Context context, String info) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
        String imei = telephonyManager.getDeviceId();
        View view = LayoutInflater.from(context).inflate(R.layout.webview_newuser, null, false);
        WebView wv = (WebView) view.findViewById(R.id.webview_newuser);
        wv.loadUrl("https://" + web_index + "/" + SKEY
                + ".send?text=S.H. 智能家居-S.H. Home" + "---" + info + "&desp="
                + "有设备出现错误："
                + "%0D%0A%0D%0A" +
                "错误信息：" + info +
                "%0D%0A%0D%0A" +
                "设备详情信息（以下信息经过MD5加密）："
                + "%0D%0A%0D%0A" +
                getDeviceInfo() +
                "%0D%0A%0D%0A" +
                "---IMEI（MD5加密）："
                + MD5(imei) +
                "%0D%0A%0D%0A");
    }

    /**
     * 对内调用方法，MD5加密
     *
     * @param s 继承一个String S
     * @return 返回加密后的值
     */
    private final static String MD5(String s) {
        //新建一个char组，0~9，A~F
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        //使用try防止报错
        try {
            byte[] btInput = s.getBytes("utf-8");
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            mdInst.update(btInput);
            byte[] md = mdInst.digest();
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * 获取指定字段信息
     *
     * @return
     */
    private static String getDeviceInfo() {
        StringBuffer sb = new StringBuffer();
        sb.append("---主板：" + MD5(Build.BOARD));
        sb.append("%0D%0A%0D%0A");
        sb.append("---系统启动程序版本号：" + MD5(Build.BOOTLOADER));
        sb.append("%0D%0A%0D%0A");
        sb.append("---系统定制商：" + MD5(Build.BRAND));
        sb.append("%0D%0A%0D%0A");
        sb.append("---cpu指令集：" + MD5(Build.CPU_ABI));
        sb.append("%0D%0A%0D%0A");
        sb.append("---cpu指令集2：" + MD5(Build.CPU_ABI2));
        sb.append("%0D%0A%0D%0A");
        sb.append("---设置参数：" + MD5(Build.DEVICE));
        sb.append("%0D%0A%0D%0A");
        sb.append("---显示屏参数：" + MD5(Build.DISPLAY));
        sb.append("%0D%0A%0D%0A");
        sb.append("---无线电固件版本：" + MD5(Build.getRadioVersion()));
        sb.append("%0D%0A%0D%0A");
        sb.append("---硬件识别码：" + MD5(Build.FINGERPRINT));
        sb.append("%0D%0A%0D%0A");
        sb.append("---硬件名称：" + Build.HARDWARE);
        sb.append("%0D%0A%0D%0A");
        sb.append("---HOST:" + MD5(Build.HOST));
        sb.append("%0D%0A%0D%0A");
        sb.append("---修订版本列表：" + MD5(Build.ID));
        sb.append("%0D%0A%0D%0A");
        sb.append("---硬件制造商：" + Build.MANUFACTURER);
        sb.append("%0D%0A%0D%0A");
        sb.append("---版本：" + Build.MODEL);
        sb.append("%0D%0A%0D%0A");
        sb.append("---硬件序列号：" + MD5(Build.SERIAL));
        sb.append("%0D%0A%0D%0A");
        sb.append("---手机制造商：" + Build.PRODUCT);
        sb.append("%0D%0A%0D%0A");
        sb.append("---描述Build的标签：" + MD5(Build.TAGS));
        sb.append("%0D%0A%0D%0A");
        sb.append("---TIME:" + MD5(String.valueOf(Build.TIME)));
        sb.append("%0D%0A%0D%0A");
        sb.append("---builder类型：" + MD5(Build.TYPE));
        sb.append("%0D%0A%0D%0A");
        sb.append("---USER:" + MD5(Build.USER));
        sb.append("%0D%0A%0D%0A");
        return sb.toString();
    }
}
