package com.etang.shtest.tools;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.etang.shtest.dialogs.DialogMessage;

/**
 * 数据库帮助类
 */
public class MySQLiteHelper {
    /**
     * 数据库查询方法
     *
     * @param context  继承context
     * @param username 用户名
     * @param passward 密码
     * @return 返回结果
     */
    public static boolean rawQuery(Context context, String username, String passward) {
        //登录结果
        boolean islogin = false;
        //数据库
        MyDataBaseHelper dbHelper = new MyDataBaseHelper(context, "info.db", null, 2);
        //数据库database帮助类
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //sql查询命令，创建cur游标
        Cursor cursor = db.rawQuery("select * from user where username = ? and passward = ?", new String[]{username, passward});
        //判断sql游标是不是空
        if (cursor.getCount() != 0) {
            //判断游标是否移动
            if (cursor.moveToNext()) {
                //登录成功
                islogin = true;
            } else {
                //登录失败
                islogin = false;
                //sql查询命令，创建cur游标
                Cursor cursor2 = db.rawQuery("select * from user where username = ?", new String[]{username});
                //判断游标中有没有数据
                if (cursor2.getCount() != 0) {
                    //获取用户名进行比对
                    if (!cursor2.getString(cursor2.getColumnIndex("username")).equals(username)) {
                        //用户名不存在
                        DiyToast.showToast(context, "错误：用户名不存在");
                    } else {
                        //获取密码进行比对
                        if (!cursor2.getString(cursor2.getColumnIndex("passward")).equals(passward)) {
                            //密码不存在
                            DiyToast.showToast(context, "错误：密码错误");
                        }
                    }
                } else {
                    //提示错误
                    DiyToast.showToast(context, "错误：离线登录数据库中还没有数据。请先注册。");
                }
            }
        } else {
            //提示错误
            DiyToast.showToast(context, "错误：离线登录数据库中还没有数据。请先注册。");
        }
        //返回结果
        return islogin;
    }

    /**
     * 数据库插入（注册）
     *
     * @param context  继承Context
     * @param username 继承String——username
     * @param passward 继承String——Passward
     */
    public static void insert(Context context, String username, String passward) {
        //数据库
        MyDataBaseHelper dbHelper = new MyDataBaseHelper(context, "info.db", null, 2);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //创建游标
        Cursor cursor = db.rawQuery("select * from user where username = ?", new String[]{username});
        //判断用户名是否已经存在
        if (cursor.moveToNext()) {
            DiyToast.showToast(context, "错误：账号" + username + "已经存在");
        } else {
            //插入数据库（注册）
            db.execSQL("insert into user (username,passward)values(?,?)", new String[]{username, passward});
            //提示
            DiyToast.showToast(context, "欢迎：" + username + "，注册成功！");
        }
    }

    /**
     * 初始化“自动模式”功能所需的数据库
     *
     * @param context 继承Context
     */
    public static void modeinsert(Context context) {
        //数据库
        MyDataBaseHelper dbHelper = new MyDataBaseHelper(context, "info.db", null, 2);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //烟雾阈值
        db.execSQL("insert into smo_value(value)values(?)", new String[]{"100"});
        //最高温度阈值
        db.execSQL("insert into temp_value_max(value)values(?)", new String[]{"100"});
        //最低温度阈值
        db.execSQL("insert into temp_value_low(value)values(?)", new String[]{"100"});
        //最高光照阈值
        db.execSQL("insert into ill_value_max(value)values(?)", new String[]{"100"});
        //最低光照阈值
        db.execSQL("insert into ill_value_low(value)values(?)", new String[]{"100"});
        //最高Co2阈值
        db.execSQL("insert into co_value_max(value)values(?)", new String[]{"100"});
        //最低Co2阈值
        db.execSQL("insert into co_value_low(value)values(?)", new String[]{"100"});
        //最高PM阈值
        db.execSQL("insert into pm_value_max(value)values(?)", new String[]{"100"});
        //最低PM阈值
        db.execSQL("insert into pm_value_low(value)values(?)", new String[]{"100"});
        //最高湿度阈值
        db.execSQL("insert into hum_value_max(value)values(?)", new String[]{"100"});
        //最低湿度阈值
        db.execSQL("insert into hum_value_low(value)values(?)", new String[]{"100"});
        //绿色模式阈值
        db.execSQL("insert into green_mode(value)values(?)", new String[]{"5"});
        //更新数据库
        update(context);
    }

    /**
     * 更新数据库
     *
     * @param context 继承Context
     */
    public static void update(Context context) {
        //数据库
        MyDataBaseHelper dbHelper = new MyDataBaseHelper(context, "info.db", null, 2);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //更新烟雾
        update_smo(db);
        //更新光照
        update_ill(db);
        //更新温度
        update_temp(db);
        //更新Co2
        update_co(db);
        //更新PM2.5
        update_pm(db);
        //更新湿度
        update_hum(db);
        //更新绿色模式
        update_green_mode(db);
    }

    /**
     * 更新绿色模式
     *
     * @param db 继承SQLiteDataBase
     */
    private static void update_green_mode(SQLiteDatabase db) {
        //新建游标，从数据库中获取绿色模式阈值
        Cursor cursor = db.rawQuery("select * from green_mode", null);
        //游标移动到第一位
        cursor.moveToFirst();
        //更新并赋值
        AppConfig.green_mode = String.valueOf(cursor.getString(cursor.getColumnIndex("value")));
    }

    /**
     * 更新烟雾
     *
     * @param db 继承SQLiteDataBase
     */
    private static void update_smo(SQLiteDatabase db) {
        //新建游标，从数据库中获取烟雾阈值
        Cursor cursor = db.rawQuery("select * from smo_value", null);
        //游标移动到第一位
        cursor.moveToFirst();
        //更新并赋值
        AppConfig.smo_value = String.valueOf(cursor.getString(cursor.getColumnIndex("value")));
    }

    /**
     * 更新温度
     *
     * @param db 继承SQLiteDataBase
     */
    private static void update_temp(SQLiteDatabase db) {
        //新建游标，从数据库中获取温度最高阈值
        Cursor cursor = db.rawQuery("select * from temp_value_max", null);
        //游标移动到第一位
        cursor.moveToFirst();
        //更新并赋值
        AppConfig.temp_value_max = String.valueOf(cursor.getString(cursor.getColumnIndex("value")));
        //关闭游标
        cursor.close();
        //从数据库中获取温度最低阈值
        cursor = db.rawQuery("select * from temp_value_low", null);
        //游标移动到第一位
        cursor.moveToFirst();
        //更新并赋值
        AppConfig.temp_value_low = String.valueOf(cursor.getString(cursor.getColumnIndex("value")));
    }

    /**
     * 更新光照
     *
     * @param db 继承SQLiteDataBase
     */
    private static void update_ill(SQLiteDatabase db) {
        //新建游标，从数据库中获取光照最高值
        Cursor cursor = db.rawQuery("select * from ill_value_max", null);
        //游标移动到第一位
        cursor.moveToFirst();
        //更新并赋值
        AppConfig.ill_value_max = String.valueOf(cursor.getString(cursor.getColumnIndex("value")));
        //关闭游标
        cursor.close();
        //新建游标，从数据库中获取光照最低值
        cursor = db.rawQuery("select * from ill_value_low", null);
        //游标移动到第一位
        cursor.moveToFirst();
        //更新并赋值
        AppConfig.ill_value_low = String.valueOf(cursor.getString(cursor.getColumnIndex("value")));
    }

    /**
     * 更新Co2
     *
     * @param db 继承SQLiteDataBase
     */
    private static void update_co(SQLiteDatabase db) {
        //新建游标，从数据库中获取Co2最高值
        Cursor cursor = db.rawQuery("select * from co_value_max", null);
        //游标移动到第一位
        cursor.moveToFirst();
        //更新并赋值
        AppConfig.co_value_max = String.valueOf(cursor.getString(cursor.getColumnIndex("value")));
        //关闭游标
        cursor.close();
        //新建游标，从数据库中获取Co2最低值
        cursor = db.rawQuery("select * from co_value_low", null);
        //游标移动到第一位
        cursor.moveToFirst();
        //更新并赋值
        AppConfig.co_value_low = String.valueOf(cursor.getString(cursor.getColumnIndex("value")));
    }

    /**
     * 更新PM2.5
     *
     * @param db 继承SQLiteDataBase
     */
    private static void update_pm(SQLiteDatabase db) {
        //新建游标，从数据库中获取PM2.5最高值
        Cursor cursor = db.rawQuery("select * from pm_value_max", null);
        //游标移动到第一位
        cursor.moveToFirst();
        //更新并赋值
        AppConfig.pm_value_max = String.valueOf(cursor.getString(cursor.getColumnIndex("value")));
        //关闭游标
        cursor.close();
        //新建游标，从数据库中获取PM2.5最低值
        cursor = db.rawQuery("select * from pm_value_low", null);
        //游标移动到第一位
        cursor.moveToFirst();
        //更新并赋值
        AppConfig.pm_value_low = String.valueOf(cursor.getString(cursor.getColumnIndex("value")));
    }

    /**
     * 更新湿度
     *
     * @param db 继承SQLiteDataBase
     */
    private static void update_hum(SQLiteDatabase db) {
        //新建游标，从数据库中获取湿度最高值
        Cursor cursor = db.rawQuery("select * from hum_value_max", null);
        //游标移动到第一位
        cursor.moveToFirst();
        //更新并赋值
        AppConfig.hum_value_max = String.valueOf(cursor.getString(cursor.getColumnIndex("value")));
        //关闭游标
        cursor.close();
        //新建游标，从数据库中获取湿度最低值
        cursor = db.rawQuery("select * from hum_value_low", null);
        //游标移动到第一位
        cursor.moveToFirst();
        //更新并赋值
        AppConfig.hum_value_low = String.valueOf(cursor.getString(cursor.getColumnIndex("value")));
    }


}
