package com.etang.shtest.tools;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.bizideal.smarthome.socket.ConstantUtil;
import com.bizideal.smarthome.socket.ControlUtils;
import com.bizideal.smarthome.socket.DataCallback;
import com.bizideal.smarthome.socket.DeviceBean;
import com.bizideal.smarthome.socket.LoginCallback;
import com.bizideal.smarthome.socket.SocketClient;
import com.bizideal.smarthome.socket.Utils.SpUtils;
import com.etang.shtest.R;
import com.etang.shtest.WeleComeActivity;
import com.etang.shtest.dialogs.DialogDebugs;
import com.etang.shtest.dialogs.DialogMessage;

import java.util.ArrayList;

/**
 * 存放软件用到的跨Activity参数
 */
public class AppConfig {
    //用户名
    public static String bizi_user = "bizideal";
    //密码
    public static String bizi_pass = "123456";
    //设备版号
    public static String BORDID_TEMP_HUM = "4";//温湿度
    public static String BORDID_ILL = "5";//光照
    public static String BORDID_SMO = "6";//烟雾
    public static String BORDID_GAS = "7";//燃气
    public static String BORDID_CO = "13";//CO2
    public static String BORDID_PM = "8";//PM2.5
    public static String BORDID_PRESS = "3";//气压
    public static String BORDID_PER = "2";//人体红外
    public static String BORDID_LAMP = "11";//射灯
    public static String BORDID_CUR = "10";//窗帘
    public static String BORDID_INF = "1";//红外
    public static String BORDID_FAN = "12";//风扇
    public static String BORDID_WARM = "9";//报警灯
    public static String BORDID_DOOR = "14";//门禁
    //传感器参数
    public static float temp = 0;//温度
    public static float hum = 0;//湿度
    public static float press = 0;//气压
    public static float smo = 0;//烟雾
    public static float gas = 0;//燃气
    public static float ill = 0;//光照
    public static float co = 0;//Co2
    public static float pm = 0;//PM2.5
    public static float per = 0;//人体红外
    //打卡参数
    public static String card_time = "";//打卡时间
    public static String card_name = "";//打卡名称
    //设备开关状态
    public static boolean lamp_state = false;//射灯状态
    public static boolean warm_state = false;//报警灯状态
    public static boolean fan_state = false;//风扇状态
    public static boolean door_state = false;//门禁状态
    public static boolean inf_tv_state = false;//红外——电视
    public static boolean inf_dvd_state = false;//红外——DVD
    public static boolean inf_kt_state = false;//红外——空调
    public static boolean cur_open_state = false;//红外——窗帘开
    public static boolean cur_cls_state = false;//红外——窗帘关
    public static boolean cur_stop_state = false;//红外——窗帘停
    /**
     * 登录模式：true为在线登录，false为离线登录
     */
    public static boolean login_links = false;
    //设备唯一标识符
    public static String phone_one_number_code = "";
    /**
     * 自动模式阈值
     */
    public static String smo_value = "";//烟雾值
    public static String temp_value_max = "";//温度值
    public static String temp_value_low = "";//温度值
    public static String ill_value_max = "";//光照值
    public static String ill_value_low = "";//光照值
    public static String co_value_max = "";//co2值
    public static String co_value_low = "";//co2值
    public static String pm_value_max = "";//pm2.5值
    public static String pm_value_low = "";//pm2.5值
    public static String hum_value_max = "";//湿度值
    public static String hum_value_low = "";//湿度值
    public static String green_mode = "";//绿色模式

    /**
     * 初始化web相关的服务
     *
     * @param activity
     */
    public static void web_init(Activity activity) {
        //开启联网
        web_links(activity);
        //检查存储权限
        SavePermission.check_save_permission(activity);
    }


    //从服务器获取参数
    private static void web_datas(Activity activity) {
        //获取数据
        ControlUtils.getData();
        //获取人脸数据
        ControlUtils.getFaceData();
        //创建链接（不知道能不能用）
        SocketClient.getInstance().creatConnect();
        //开始从服务器获取数据
        SocketClient.getInstance().getData(new DataCallback<DeviceBean>() {
            @Override
            public void onResult(final DeviceBean getdata) {
                //新建可操作UI的线程
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //使用try防止报错闪退
                        try {
                            //获取打卡ID
                            if (!TextUtils.isEmpty(getdata.getName())) {
                                AppConfig.card_name = getdata.getName();
                            }
                            //获取打卡时间
                            if (!TextUtils.isEmpty(getdata.getTime())) {
                                AppConfig.card_time = getdata.getTime();
                            }
                            if (getdata.getDevice().size() > 0) {
                                DiyToast.showToast(activity, "当前已装载：" + getdata.getDevice().size() + "个设备");
                                for (int i = 0; i < getdata.getDevice().size(); i++) {
                                    /**
                                     * 获取传感器状态
                                     */
                                    //获取温湿度
                                    if (getdata.getDevice().get(i).getBoardId().equals(BORDID_TEMP_HUM)) {
                                        if (getdata.getDevice().get(i).getSensorType().equals(ConstantUtil.Temperature)) {
                                            //温度
                                            temp = Float.valueOf(getdata.getDevice().get(i).getValue());
                                        } else {
                                            //湿度
                                            hum = Float.valueOf(getdata.getDevice().get(i).getValue());
                                        }
                                    }
                                    if (getdata.getDevice().get(i).getBoardId().equals(BORDID_ILL)) {//光照度
                                        ill = Float.valueOf(getdata.getDevice().get(i).getValue());
                                    }
                                    if (getdata.getDevice().get(i).getBoardId().equals(BORDID_SMO)) {//烟雾
                                        smo = Float.valueOf(getdata.getDevice().get(i).getValue());
                                    }
                                    if (getdata.getDevice().get(i).getBoardId().equals(BORDID_GAS)) {//燃气
                                        gas = Float.valueOf(getdata.getDevice().get(i).getValue());
                                    }
                                    if (getdata.getDevice().get(i).getBoardId().equals(BORDID_CO)) {//Co2
                                        co = Float.valueOf(getdata.getDevice().get(i).getValue());
                                    }
                                    if (getdata.getDevice().get(i).getBoardId().equals(BORDID_PM)) {//pm2.5
                                        pm = Float.valueOf(getdata.getDevice().get(i).getValue());
                                    }
                                    if (getdata.getDevice().get(i).getBoardId().equals(BORDID_PRESS)) {//气压
                                        press = Float.valueOf(getdata.getDevice().get(i).getValue());
                                    }
                                    if (getdata.getDevice().get(i).getBoardId().equals(BORDID_PER)) {//人体红外
                                        if (!TextUtils.isEmpty(getdata.getDevice().get(i).getValue()) && !getdata.getDevice().get(i).getValue().equals(ConstantUtil.Close)) {
                                            per = 1;
                                        } else {
                                            per = 0;
                                        }
                                    }
                                    /**
                                     * 获取设备状态
                                     */
//                                    if (getdata.getDevice().get(i).getBoardId().equals(BORDID_LAMP)) {//射灯 1，2
//                                        if (!TextUtils.isEmpty(getdata.getDevice().get(i).getValue())
//                                                && !getdata.getDevice().get(i).getValue().equals(ConstantUtil.Close)) {
//                                            lamp_state = false;
//                                        } else {
//                                            lamp_state = true;
//                                        }
//                                    }
//                                    if (getdata.getDevice().get(i).getBoardId().equals("11")) {//窗帘（没有开关状态的话没必要写）
//                                        if (!TextUtils.isEmpty(getdata.getDevice().get(i).getValue())
//                                                && !getdata.getDevice().get(i).getValue().equals(ConstantUtil.Close)) {
//                                            lamp_state = false;
//                                        } else {
//                                            lamp_state = true;
//                                        }
//                                    }
                                    if (getdata.getDevice().get(i).getBoardId().equals("1")) {//红外发射
                                        if (!TextUtils.isEmpty(getdata.getDevice().get(i).getValue())
                                                && !getdata.getDevice().get(i).getValue().equals(ConstantUtil.Close)) {
                                            lamp_state = false;
                                        } else {
                                            lamp_state = true;
                                        }
                                    }
                                    if (getdata.getDevice().get(i).getBoardId().equals(BORDID_FAN)) {//换气扇
                                        if (!TextUtils.isEmpty(getdata.getDevice().get(i).getValue())
                                                && !getdata.getDevice().get(i).getValue().equals(ConstantUtil.Close)) {
                                            fan_state = false;
                                        } else {
                                            fan_state = true;
                                        }
                                    }
                                    if (getdata.getDevice().get(i).getBoardId().equals(BORDID_WARM)) {//报警灯
                                        if (!TextUtils.isEmpty(getdata.getDevice().get(i).getValue())
                                                && !getdata.getDevice().get(i).getValue().equals(ConstantUtil.Close)) {
                                            warm_state = false;
                                        } else {
                                            warm_state = true;
                                        }
                                    }
                                    if (getdata.getDevice().get(i).getBoardId().equals(BORDID_DOOR)) {//门禁
                                        if (!TextUtils.isEmpty(getdata.getDevice().get(i).getValue())
                                                && !getdata.getDevice().get(i).getValue().equals(ConstantUtil.Close)) {
                                            door_state = false;
                                        } else {
                                            door_state = true;
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            //输出异常信息
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    //连接服务器
    private static void web_links(Activity activity) {
        //新建带进度条的dialog
        ProgressDialog progressDialog = new ProgressDialog(activity);
        //设置dialog的title标题
        progressDialog.setTitle("连接服务器中......");
        //设置dialog的图标
        progressDialog.setIcon(R.drawable.ic_offline);
        //设置dialog的信息
        progressDialog.setMessage("连接IP：" + SocketClient.ip + "中，请稍后......");
        //设置点击dialog外部区域可否被关闭
        progressDialog.setCanceledOnTouchOutside(false);
        //显示dialog
        progressDialog.show();
        //设置用户名、密码、IP地址
        ControlUtils.setUser(bizi_user, bizi_pass, SocketClient.ip);
        //输出IP地址用于确认是否正确
        Log.e("Bizideal-Socket-IP", SocketClient.ip);
        //创建链接
        SocketClient.getInstance().creatConnect();
        //获取链接状态
        SocketClient.getInstance().login(new LoginCallback() {
            @Override
            public void onEvent(final String s) {
                //新建可操作UI的线程
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (s.equals(ConstantUtil.Success)) {
                            //成功
                            login_links = true;
                            DiyToast.showToast(activity, activity.getString(R.string.web_link_success));
                            progressDialog.dismiss();
                            web_datas(activity);
                        } else if (s.equals(ConstantUtil.Failure)) {
                            //失败
                            login_links = false;
                            DiyToast.showToast(activity, activity.getString(R.string.web_link_error));
                            progressDialog.dismiss();
                            DialogDebugs.showdialog(activity, activity.getString(R.string.web_link_error), "Error IP ：" + SocketClient.ip);
                            web_datas(activity);
                        } else if (s.equals(ConstantUtil.Reconnect)) {
                            //重新连接
                            login_links = false;
                            DiyToast.showToast(activity, activity.getString(R.string.web_link_ReLink));
                            web_datas(activity);
                        } else {
                            //出现未知错误
                            login_links = false;
                            DiyToast.showToast(activity, activity.getString(R.string.web_link_UnKownError));
                            progressDialog.dismiss();
                            DialogDebugs.showdialog(activity, "未知错误", "连接服务器发生未知错误");
                            web_datas(activity);
                        }
                    }
                });
            }
        });
    }


    public static void getBORDID(Context context) {
        /**
         * 顺序一般是：
         * 1.温度\湿度
         * 2.烟雾
         * 3.燃气
         * 4.气压
         * 5.人体红外
         * 6.Co2
         * 7.PM25
         * 8.光照
         * 9.射灯
         * 10.风扇
         * 11.报警灯
         * 12.门禁
         * 13.窗帘
         * 14.红外转发
         */
        try {
            BORDID_TEMP_HUM = SpUtils.getValue(context, "bordid_temp", null);//温度、湿度
            BORDID_SMO = SpUtils.getValue(context, "bordid_smo", null);//烟雾
            BORDID_GAS = SpUtils.getValue(context, "bordid_gas", null);//燃气
            BORDID_PRESS = SpUtils.getValue(context, "bordid_press", null);//气压
            BORDID_PER = SpUtils.getValue(context, "bordid_per", null);//人体红外
            BORDID_CO = SpUtils.getValue(context, "bordid_co", null);//Co2
            BORDID_PM = SpUtils.getValue(context, "bordid_pm", null);//pm2.5
            BORDID_ILL = SpUtils.getValue(context, "bordid_ill", null);//光照
            BORDID_LAMP = SpUtils.getValue(context, "bordid_lamp", null);//射灯
            BORDID_FAN = SpUtils.getValue(context, "bordid_fan", null);//风扇
            BORDID_WARM = SpUtils.getValue(context, "bordid_warm", null);//报警灯
            BORDID_DOOR = SpUtils.getValue(context, "bordid_door", null);//门禁
            BORDID_CUR = SpUtils.getValue(context, "bordid_cur", null);//窗帘
            BORDID_INF = SpUtils.getValue(context, "bordid_redsend", null);//红外转发
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setBORDID(Activity activity, Context context, ArrayList<String> strings) {
        /**
         * 顺序一般是：
         * 1.温度、湿度
         * 2.烟雾
         * 3.燃气
         * 4.气压
         * 5.人体红外
         * 6.Co2
         * 7.PM25
         * 8.光照
         * 9.射灯
         * 10.风扇
         * 11.报警灯
         * 12.门禁
         * 13.窗帘
         * 14.红外转发
         */
        //判断list中的数量，当数量少于14，则是缺少部分设备的版号（共14个设备需要设置版号）
        if (strings.size() < 14 || strings.size() > 14) {
            //弹出报错
            DialogDebugs.showdialog(activity, "AppConfig.java", "BordID size < 14");
        } else {
            //使用try防止报错闪退
            try {
                //数量较多，使用线程
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //设置温湿度版号
                        BORDID_TEMP_HUM = strings.get(0);
                        //设置烟雾版号
                        BORDID_SMO = strings.get(1);
                        //设置燃气版号
                        BORDID_GAS = strings.get(2);
                        //设置气压版号
                        BORDID_PRESS = strings.get(3);
                        //设置人体红外版号
                        BORDID_PER = strings.get(4);
                        //设置二氧化碳版号
                        BORDID_CO = strings.get(5);
                        //设置PM2.5版号
                        BORDID_PM = strings.get(6);
                        //设置光照版号
                        BORDID_ILL = strings.get(7);
                        //设置射灯版号
                        BORDID_LAMP = strings.get(8);
                        //设置风扇版号
                        BORDID_FAN = strings.get(9);
                        //设置报警灯版号
                        BORDID_WARM = strings.get(10);
                        //设置门禁版号
                        BORDID_DOOR = strings.get(11);
                        //设置窗帘版号
                        BORDID_CUR = strings.get(12);
                        //设置红外转发版号
                        BORDID_INF = strings.get(13);
                        //设置
                        initBORDID(context);
                    }
                }).start();
            } catch (Exception e) {
                //弹出报错
                DialogDebugs.showdialog(activity, "AppConfig.java", "BordID size < 14");
            }
        }
    }

    /**
     * 初始化设备版号
     *
     * @param context 继承context
     */
    public static void initBORDID(Context context) {
        /**
         * 顺序一般是：
         * 1.温度\湿度
         * 2.烟雾
         * 3.燃气
         * 4.气压
         * 5.人体红外
         * 6.Co2
         * 7.PM25
         * 8.光照
         * 9.射灯
         * 10.风扇
         * 11.报警灯
         * 12.门禁
         * 13.窗帘
         * 14.红外转发
         */
        SpUtils.putValue(context, "bordid_temp", BORDID_TEMP_HUM);//温度、湿度
        Log.i("AppConfig-APP配置加载", "initBORDID-初始化温度版号: " + BORDID_TEMP_HUM);
        SpUtils.putValue(context, "bordid_smo", BORDID_SMO);//烟雾
        Log.i("AppConfig-APP配置加载", "initBORDID-初始化烟雾版号: " + BORDID_SMO);
        SpUtils.putValue(context, "bordid_gas", BORDID_GAS);//燃气
        Log.i("AppConfig-APP配置加载", "initBORDID-初始化燃气版号: " + BORDID_GAS);
        SpUtils.putValue(context, "bordid_press", BORDID_PRESS);//气压
        Log.i("AppConfig-APP配置加载", "initBORDID-初始化气压版号: " + BORDID_PRESS);
        SpUtils.putValue(context, "bordid_per", BORDID_PER);//人体红外
        Log.i("AppConfig-APP配置加载", "initBORDID-初始化人体红外版号: " + BORDID_PER);
        SpUtils.putValue(context, "bordid_co", BORDID_CO);//Co2
        Log.i("AppConfig-APP配置加载", "initBORDID-初始化Co2版号: " + BORDID_CO);
        SpUtils.putValue(context, "bordid_pm", BORDID_PM);//pm2.5
        Log.i("AppConfig-APP配置加载", "initBORDID-初始化PM2.5版号: " + BORDID_PM);
        SpUtils.putValue(context, "bordid_ill", BORDID_ILL);//光照
        Log.i("AppConfig-APP配置加载", "initBORDID-初始化光照版号: " + BORDID_ILL);
        SpUtils.putValue(context, "bordid_lamp", BORDID_LAMP);//射灯
        Log.i("AppConfig-APP配置加载", "initBORDID-初始化射灯版号: " + BORDID_LAMP);
        SpUtils.putValue(context, "bordid_fan", BORDID_FAN);//风扇
        Log.i("AppConfig-APP配置加载", "initBORDID-初始化风扇版号: " + BORDID_FAN);
        SpUtils.putValue(context, "bordid_warm", BORDID_WARM);//报警灯
        Log.i("AppConfig-APP配置加载", "initBORDID-初始化报警灯版号: " + BORDID_WARM);
        SpUtils.putValue(context, "bordid_door", BORDID_DOOR);//门禁
        Log.i("AppConfig-APP配置加载", "initBORDID-初始化门禁版号: " + BORDID_DOOR);
        SpUtils.putValue(context, "bordid_cur", BORDID_CUR);//窗帘
        Log.i("AppConfig-APP配置加载", "initBORDID-初始化窗帘版号: " + BORDID_CUR);
        SpUtils.putValue(context, "bordid_redsend", BORDID_INF);//红外转发
        Log.i("AppConfig-APP配置加载", "initBORDID-初始化红外转发版号: " + BORDID_INF);
        //获取信息
        getBORDID(context);
    }
}
