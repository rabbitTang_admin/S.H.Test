package com.etang.shtest.tools;

import android.content.Context;
import android.widget.Toast;

/**
 * 自定义Toast
 */
public class DiyToast {
    //新建toast
    private static Toast toast;

    /**
     * 显示Toast的方法
     *
     * @param context 继承Context
     * @param s       继承要显示的字符串
     */
    public static void showToast(Context context, String s) {
        //判断toast是不是空，即toast有没有被创建\显示
        if (toast == null) {
            //空，就创建一个Toast
            toast = Toast.makeText(context, s, Toast.LENGTH_SHORT);
        } else {
            //不是空，设置toast内的文本（应该是重新显示）
            toast.setText(s);
        }
        //显示Toast
        toast.show();
    }
}
