package com.etang.shtest.tools;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.Random;

/**
 * 生成一个随机验证码
 */
public class CodeView {
    //字符串
    private static String code_all = "1234567890qweioywetiuygufadvbhjasgiutgQIUPOWRYIUWQETPO1234567890qweioywetiuygufadvbhjasgiutgQIUPOWRYIUWQETPOSADTOIUFTGHJZVCM1234567980";
    //最终输出的验证码字符串
    public static String code = "";

    /**
     * 创建bitmap
     *
     * @return 返回bitmap
     */
    public static Bitmap createBitmap(int width, int height) {
        //新建一个bitmap，高50宽140。
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        //新建随机数
        Random random = new Random();
        //新建画布，画布继承bitmap
        Canvas canvas = new Canvas(bitmap);
        //新建画笔
        Paint paint = new Paint();
        //新建文本大小
        paint.setTextSize(18);
        //新建StringBuffer
        StringBuffer stringBuffer = new StringBuffer();
        //新建for循环
        for (int i = 0; i < 4; i++) {
            //增加验证码随机字
            stringBuffer.append(code_all.charAt(random.nextInt(code_all.length())));
        }
        //给code赋值
        code = stringBuffer.toString();
        //默认左边距
        int base_left = 20;
        //默认顶边距
        int base_top = 20;
        //随机左边距
        int random_left = 20;
        //随机顶边距
        int random_top = 20;
        //for循环，次数为code的字符串数量
        for (int i = 0; i < code.length(); i++) {
            //设置随机颜色
            paint.setColor(Color.rgb(random.nextInt(256), random.nextInt(256), random.nextInt(256)));
            //设置随机抗锯齿
            paint.setAntiAlias(random.nextBoolean());
            //设置字体随机加粗
            paint.setFakeBoldText(random.nextBoolean());
            //设置字体随机倾斜
            paint.setTextSkewX(-random.nextInt(1));
            //赋值随机顶边距随机数
            random_top = base_top + random.nextInt(10);
            //绘制文本
            canvas.drawText(code.charAt(i) + "", base_left, random_top, paint);
            //设置随机左边距
            base_left += random_left + random.nextInt(10);
        }
        //随机绘制一条线
        canvas.drawLine(random.nextInt(canvas.getWidth()), random.nextInt(canvas.getHeight()), random.nextInt(canvas.getWidth()), random.nextInt(canvas.getHeight()), paint);
        //保存画布
        canvas.save();
        //重置画布
        canvas.restore();
        //返回bitmap
        return bitmap;
    }

    /**
     * 创建bitmap
     *
     * @return 返回bitmap
     */
    public static Bitmap createUserNameBitmap(String username, int width, int height) {
        //新建一个bitmap，高50宽140。
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        //新建随机数
        Random random = new Random();
        //新建画布，画布继承bitmap
        Canvas canvas = new Canvas(bitmap);
        //新建画笔
        Paint paint = new Paint();
        //新建文本大小
        paint.setTextSize(18);
        //新建StringBuffer
        StringBuffer stringBuffer = new StringBuffer();
        //默认左边距
        int base_left = 20;
        //for循环，次数为onecode的字符串数量
        for (int i = 0; i < username.length(); i++) {
            //设置随机颜色
            paint.setColor(Color.rgb(random.nextInt(256), random.nextInt(256), random.nextInt(256)));
            //设置随机抗锯齿
            paint.setAntiAlias(random.nextBoolean());
            //绘制文本
            canvas.drawText(username.charAt(i) + "", base_left, height, paint);
            //设置随机左边距
            base_left += base_left;
        }
        //保存画布
        canvas.save();
        //重置画布
        canvas.restore();
        //返回bitmap
        return bitmap;
    }

    /**
     * 创建bitmap
     *
     * @return 返回bitmap
     */
    public static Bitmap createOneCodeBitmap(int width, int height) {
        //新建一个bitmap，高50宽140。
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        //新建随机数
        Random random = new Random();
        //新建画布，画布继承bitmap
        Canvas canvas = new Canvas(bitmap);
        //新建画笔
        Paint paint = new Paint();
        //新建文本大小
        paint.setTextSize(18);
        //新建StringBuffer
        StringBuffer stringBuffer = new StringBuffer();
        //新建for循环
        for (int i = 0; i < 9; i++) {
            //增加验证码随机字
            stringBuffer.append(code_all.charAt(random.nextInt(code_all.length())));
        }
        //给code赋值
        final String onecode = stringBuffer.toString();
        //默认左边距
        int base_left = 20;
        //for循环，次数为onecode的字符串数量
        for (int i = 0; i < onecode.length(); i++) {
            //设置随机颜色
            paint.setColor(Color.rgb(random.nextInt(256), random.nextInt(256), random.nextInt(256)));
            //设置随机抗锯齿
            paint.setAntiAlias(random.nextBoolean());
            //绘制文本
            canvas.drawText(onecode.charAt(i) + "", base_left, height, paint);
            //设置随机左边距
            base_left += base_left;
        }
        //保存画布
        canvas.save();
        //重置画布
        canvas.restore();
        //返回bitmap
        return bitmap;
    }
}
