## 多言語開発ドキュメント支援

| 言語          | リンク                                                        | バージョン |
| -------------- | ------------------------------------------------------------ | ---- |
| 英語.\|English  | [English VERSION](https://gitee.com/rabbitTang_admin/S.H.Test/blob/master/README_ENG.md) | v1.0 |
| 中国語.\|Chinese  | [中国語. VERSION](https://gitee.com/rabbitTang_admin/S.H.Test/blob/master/README.md) | v1.0 |
| 日本語\|Japanese | [日本語 VERSION](https://gitee.com/rabbitTang_admin/S.H.Test/blob/master/README_JPN.md) | v1.0 |

## オープンソースについては

本プロジェクトは，``2020年11月11日``オープンソース，*[GPL-2.0](https：//gitee.com/rabbitTang_admin/S.H.Test/blob/master/LICENSE)**ライセンスを用いて，それぞれ必要なものを取得した.

## クロスプラットフォームについて

~~本プロジェクトは，Mac，Linux，Windowsなどのプラットフォームを理論的にサポートする`Java`と`C++`のハイブリッドプログラミングによりプラットフォームを実現している.~~


クロスプラットフォームを一時的に放棄する(多くのAPIは機能を実現するために書き換えが必要であり、後日再メンテナンスする暇がある)

## S.H.HOME

Bizidealスマートホームインストールと保守プラットフォームAndroid側の制御端末ソフトウェアに基づいています。

## ユーザ文書.

編纂中.

## ほめる

| WeChat                                                         | アリペイ.                                                       |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![微信](https://yp.nyanon.online/index.php?user/publicLink&fid=2de0cWutSBNLwAHf2wKj9jTu1p7CdgvG7frqYDYx5hxy0-olktzX-pqFUu_ERrtigTfFQjOA_v5-DVtk-XrE9aiIZfLKXOIVys5HDdhstpaNSyzMXOPdMC4fJ9QMaESHcnjusI71vhlwO-Zcn2a4&file_name=/wechatpay.jpg) | ![支付宝](https://yp.nyanon.online/index.php?user/publicLink&fid=fbc5CTYExmGz2jwK5RCh_7AH_VBreib4I37_naefmWF-PijPGScnC933sBCbb8IG8t7hjKBiFyj6Ej74mk6dyu5ZrrzqRzyOXmmQKEzesejamTV9YZm62xS9TstSW0t2iPwyQGPJeJMXgGXA&file_name=/alipay.jpg) |





