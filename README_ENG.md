## Multilingual development documentation support

| Language           | Link                                                         | Version |
| -------------- | ------------------------------------------------------------ | ---- |
| 英语\|English  | [English VERSION](https://gitee.com/rabbitTang_admin/S.H.Test/blob/master/README_ENG.md) | v1.0 |
| 中文\|Chinese  | [Chinese VERSION](https://gitee.com/rabbitTang_admin/S.H.Test/blob/master/README.md) | v1.0 |
| 日本語\|Japanese | [Japanese VERSION](https://gitee.com/rabbitTang_admin/S.H.Test/blob/master/README_JPN.md) | v1.0 |

## About Open Source

This project has been open source with ``November 11,2020``, using * * [GPL-2.0] (https://gitee.com/rabbitTang_admin/S.H.Test/blob/master/LICENSE)** license), each takes what it needs.

## About cross-platform

~~This project has realized cross-platform through mixed programming of `Java` and `C++', and supports Mac, Linux, Windows and other platforms in theory.~~


Temporarily abandon cross-platform (many API need to be rewritten to achieve function, and will be maintained later)

## S.H.HOME

Control terminal equipment software based on Android side of Bizideal smart home installation and maintenance platform.

## User documentation

Compiling

## Appreciate

| WeChat                                                         | Alipay                                                       |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![微信](https://yp.nyanon.online/index.php?user/publicLink&fid=2de0cWutSBNLwAHf2wKj9jTu1p7CdgvG7frqYDYx5hxy0-olktzX-pqFUu_ERrtigTfFQjOA_v5-DVtk-XrE9aiIZfLKXOIVys5HDdhstpaNSyzMXOPdMC4fJ9QMaESHcnjusI71vhlwO-Zcn2a4&file_name=/wechatpay.jpg) | ![支付宝](https://yp.nyanon.online/index.php?user/publicLink&fid=fbc5CTYExmGz2jwK5RCh_7AH_VBreib4I37_naefmWF-PijPGScnC933sBCbb8IG8t7hjKBiFyj6Ej74mk6dyu5ZrrzqRzyOXmmQKEzesejamTV9YZm62xS9TstSW0t2iPwyQGPJeJMXgGXA&file_name=/alipay.jpg) |





